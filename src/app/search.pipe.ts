import {Pipe, PipeTransform} from '@angular/core';
import {City} from "./app.component";

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(cities: City[], searchText: string): City[] {
    return searchText ?
      cities.filter(city => SearchPipe.clearAccentChars(city.name).toLocaleLowerCase().trim().indexOf(SearchPipe.clearAccentChars(searchText).toLocaleLowerCase().trim()) !== -1)
      : cities;
  }

  private static clearAccentChars(text: string): string {
    return text.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

}
