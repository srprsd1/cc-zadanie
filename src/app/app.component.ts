import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons'
import {finalize, switchMap} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  faMapMarkerAlt = faMapMarkerAlt;
  cities: City[] = cities.sort((a, b) => a.name.localeCompare(b.name));
  selectedCity = cities[0];
  selectedCityData!: WeatherData;
  dailyData!: OneCallData;
  weatherUrl = 'http://api.openweathermap.org/data/2.5/weather';
  oneCallUrl = 'http://api.openweathermap.org/data/2.5/onecall';
  groupUrl = 'http://api.openweathermap.org/data/2.5/group';
  isLoading = true;
  isLocationView = false;
  searchValue = '';

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.getWeatherData(this.selectedCity);
    this.getGroupData();
  }

  getWeatherData(city: any) {
    this.isLoading = true;
    this.http.get<WeatherData>(this.weatherUrl, {
      params: {
        id: city.id,
      }
    }).pipe(
      switchMap(data => {
        this.selectedCityData = data;
        return this.http.get<OneCallData>(this.oneCallUrl, {
          params: {
            lat: data.coord.lat,
            lon: data.coord.lon,
            exclude: 'hourly,minutely'
          }
        });
      }), finalize(() => this.isLoading = false))
      .subscribe(data => this.dailyData = data);
  }

  getGroupData() {
    this.http.get<GroupData>(this.groupUrl, {
      params: {
        id: cities.map(city => city.id).toString(),
      }
    }).subscribe(groupData => {
      this.cities.map(city => {
        let cityData = groupData.list.find(el => el.id === city.id);
        city.temp = cityData!.main.temp;
        return city;
      });
    });
  }

  toggleLocationView(): void {
    this.isLocationView = !this.isLocationView;
  }

  selectCity(city: City): void {
    this.toggleLocationView();
    this.selectedCity = city;
    this.getWeatherData(city);
  }


}

const cities = [
  {
    name: 'Košice',
    id: 865084,
    temp: 0
  },
  {
    name: 'Bratislava',
    id: 3060972,
    temp: 0
  },
  {
    name: 'Koromľa',
    id: 690548,
    temp: 0
  },
  {
    name: 'Michalovce',
    id: 724144,
    temp: 0
  },
  {
    name: 'Sobrance',
    id: 723554,
    temp: 0
  },
  {
    name: 'Humenné',
    id: 724627,
    temp: 0
  }
];

export interface City {
  id: number;
  name: string;
  temp: number;
}

interface GroupData {
  list: [{
    id: number,
    main: {
      temp: number
    }
  }];
}

interface WeatherData {
  id: number;
  coord: {
    lat: number;
    lon: number;
  }
  main: {
    temp: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number;
  };
  dt: number;
  wind: {
    speed: number;
  };
  sys: {
    sunrise: number;
    sunset: number;
    visibility: number;
  };
  weather: [
    {
      main: string;
    }
  ];
}

interface OneCallData {
  daily: [
    {
      dt: number;
      temp: {
        min: number;
        max: number;
      }
    }
  ]
}
